import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobprog/AcceptScreen.dart';
import 'package:mobprog/AddGroup.dart';
import 'package:mobprog/FileManager.dart';
import 'package:mobprog/Login.dart';

final Firestore firestore = Firestore.instance;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CELASTROOM',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        accentColor: Colors.orangeAccent,
        primaryColor: Colors.grey[900],
        textTheme: TextTheme(
          body1: TextStyle(color: Colors.white),
        ),
      ),
      // home: AcceptScreen());
      home: MyLogin(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String _inputtedCode = "";
  String _userId = '';

  final TextEditingController controller = new TextEditingController();
  @override
  void initState() {
    super.initState();

    getUserId();
  }

  void getUserId() async {
    final user = await FirebaseAuth.instance.currentUser();

    this._userId = user.uid;
  }

  void _onSearch() async {
    final user = await FirebaseAuth.instance.currentUser();

    firestore
        .collection("group")
        .where("kode", isEqualTo: _inputtedCode)
        .where('member', arrayContains: user.uid)
        .getDocuments()
        .then((data) {
      if (data.documents.isNotEmpty) {
        print("SUDAH JOIN");
      } else {
        firestore
            .collection("group")
            .where("kode", isEqualTo: _inputtedCode)
            .getDocuments()
            .then((data) {
          setState(() {
            _inputtedCode = "";
          });
          controller.clear();
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AcceptScreen(data.documents[0])));
        }).catchError((error) {
          print(error);
        });
      }
    }).catchError((error) {
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Card(
                color: Colors.grey[800],
                elevation: 0,
                margin: EdgeInsets.all(16),
                child: TextField(
                  autofocus: false,
                  controller: controller,
                  textCapitalization: TextCapitalization.characters,
                  style: TextStyle(color: Colors.white),
                  onChanged: (text) => {
                    setState(() {
                      _inputtedCode = text;
                    })
                  },
                  onSubmitted: (text) => {_onSearch()},
                  decoration: InputDecoration(
                      hintText: "Input Group Code...",
                      hintStyle: TextStyle(color: Colors.white60),
                      border: OutlineInputBorder(),
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.search,
                          color: Colors.white60,
                        ),
                        onPressed: _onSearch,
                      )),
                ),
              ),
              Expanded(
                child: StreamBuilder(
                    stream: firestore
                        .collection("group")
                        .where("member", arrayContains: _userId)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Text("NOT FOUND");
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data.documents == null
                              ? 0
                              : snapshot.data.documents.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot ds =
                                snapshot.data.documents[index];

                            return Container(
                              key: ValueKey(ds.documentID),
                              padding: EdgeInsets.fromLTRB(16, 90, 16, 90),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                      width: 1.0, color: Colors.white24),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "${ds["nama_group"]}",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  OutlineButton(
                                    borderSide: BorderSide(color: Colors.white),
                                    child: Text(
                                      "DETAILS",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () => {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => MyFiles()),
                                      )
                                    },
                                  )
                                ],
                              ),
                            );
                          },
                        );
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddGroup()),
          )
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
