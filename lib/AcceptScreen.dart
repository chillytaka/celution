import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

final Firestore firestore = Firestore.instance;

class AcceptScreen extends StatefulWidget {
  AcceptScreen(this.documentSnapshot);

  final DocumentSnapshot documentSnapshot;

  @override
  _AcceptScreenState createState() => _AcceptScreenState();
}

class _AcceptScreenState extends State<AcceptScreen> {
  void _onJoin() async {
    List<String> member = [];
    member = widget.documentSnapshot.data["member"];
    final userId = await FirebaseAuth.instance.currentUser();

    if (member == null) {
      member = [];
    }
    member.add(userId.uid);

    firestore
        .collection("group")
        .document(widget.documentSnapshot.documentID)
        .updateData({"member": member}).then((data) {
      Navigator.pop(context);
    }).catchError((error) {
      print(error);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Do You Want to Join ?",
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 80.0,
              ),
              Text(
                "${widget.documentSnapshot.data["nama_group"]}",
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              MaterialButton(
                child: Text("ACCEPT"),
                onPressed: _onJoin,
                color: Theme.of(context).accentColor,
              ),
              OutlineButton(
                borderSide: BorderSide(color: Colors.white),
                child: Text(
                  "CANCEL",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => {Navigator.pop(context)},
                color: Theme.of(context).accentColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
