import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:random_string/random_string.dart';

final Firestore firestore = Firestore.instance;

class AddGroup extends StatefulWidget {
  @override
  _AddGroupState createState() => _AddGroupState();
}

class _AddGroupState extends State<AddGroup> {
  String _group_name = "";
  String _kode = '';
  String _final_name = '';

  final TextEditingController textEditingController =
      new TextEditingController();

  void _onCreate() async {
    String kode = randomAlphaNumeric(5).toUpperCase();
    final userId = await FirebaseAuth.instance.currentUser();

    await firestore.collection("group").add({
      'nama_group': _group_name,
      'kode': kode,
      'admin': userId.uid,
      'member': [userId.uid]
    });

    textEditingController.clear();

    setState(() {
      _final_name = _group_name;
      _kode = kode;
    });
  }

  Widget _result() {
    if (_kode != '') {
      return Column(
        children: <Widget>[
          Text(
            'success',
            style: TextStyle(color: Colors.green, fontSize: 15),
          ),
          Text(
            'Nama Group : $_final_name',
            style: TextStyle(fontSize: 15),
          ),
          Text(
            '$_kode',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          Text(
            'paste kode diatas, dan sebarkan ke teman, keluarga, dan relasi anda',
            style: TextStyle(color: Colors.white60),
            textAlign: TextAlign.center,
          ),
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Text(
            "",
            style: TextStyle(fontSize: 15),
          ),
          Text(
            "",
            style: TextStyle(fontSize: 15),
          ),
          Text(
            "",
            style: TextStyle(fontSize: 20),
          ),
          Text(
            "",
          )
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Theme.of(context).primaryColor,
        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Create New Group",
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              _result(),
              SizedBox(
                height: 40,
              ),
              TextField(
                autofocus: false,
                controller: textEditingController,
                onChanged: (text) => {
                  setState(() {
                    _group_name = text;
                  })
                },
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  labelText: "Group Name",
                  labelStyle: TextStyle(color: Colors.white60),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1.0, color: Colors.white24),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1.0, color: Colors.white24),
                  ),
                ),
              ),
              SizedBox(
                height: 80,
              ),
              MaterialButton(
                child: Text("CREATE"),
                color: Theme.of(context).accentColor,
                onPressed: _onCreate,
              ),
              OutlineButton(
                borderSide: BorderSide(color: Colors.white),
                child: Text(
                  "CANCEL",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () => {Navigator.pop(context)},
                color: Theme.of(context).accentColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
